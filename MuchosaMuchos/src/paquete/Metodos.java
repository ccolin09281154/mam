/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.util.Collection;
import java.util.Date;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Metodos {

    public int d, ide, idd;
    public String n, a, p, nd;
    Scanner scan = new Scanner(System.in);

    public void inserta() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Producto producto = new Producto();
        Orden or = new Orden();
        Date utilDate = new Date();
        java.sql.Date SQLdate = new java.sql.Date(utilDate.getTime());

        Lineaorden ln = new Lineaorden();
        int op;
        do {
            System.out.println("1)Insertar Producto");
            System.out.println("2)Insertar Orden");
            System.out.println("3)Insertar LineaOrden");
            op = scan.nextInt();
            switch (op) {
                case 1:

                    producto.setDescripcion("sprite");
                    producto.setPrecio(11.5f);
                    em.persist(producto);
                    System.out.println("producto insertado" + "Descripcion:" + producto.getDescripcion() + "\t" + "Precio" + producto.getPrecio());
                    break;
                case 2:

                    or.setFechaOrden(SQLdate);
                    //or= em.find(Orden.class, idd);
                    em.persist(or);
                    System.out.println("la Orden insertada es en la fecha de:" + or + or.getFechaOrden());
                    break;
                case 3:
                    ln.setProducto(producto);
                    ln.setOrden(or);
                    ln.setCantidad(34);
                    em.persist(ln);
                    System.out.println("la linea de orden es insertada con la cantidad" + ln.getCantidad());
                    break;
                default:
                    System.out.println("Opcion no Valida.");
                    break;
            }
        } while (op != 3 && op != 2 && op != 1);
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        em.close();
        emf.close();
    }

    public void muestra() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        int op;
        do {
            System.out.println("1)Mostrar Productos");
            System.out.println("2)Mostrar  Orden");
            System.out.println("3)Mostrar linea Orden");

            op = scan.nextInt();
            switch (op) {
                case 1:
                    Collection<Producto> lista_producto;
                    lista_producto = em.createNamedQuery("Producto.findAll").getResultList();
                    for (Producto el : lista_producto) {
                        System.out.println("Id: " + el.getIdProducto() + " Descripcion: " + el.getDescripcion() + " Precio: " + el.getPrecio());
                    }
                    break;
                case 2:
                    Collection<Orden> orden;
                    orden = em.createNamedQuery("Orden.findAll").getResultList();
                    for (Orden el : orden) {
                        System.out.println("IdOrden: " + el.getIdOrden() + " Fecha de Orden: " + el.getFechaOrden());
                    }
                    break;
                case 3:
                    Collection<Lineaorden> lista_orden;
                    lista_orden = em.createNamedQuery("Lineaorden.findAll").getResultList();
                    for (Lineaorden el : lista_orden) {
                        System.out.println("IdOrden: " + el.getOrden() + " Idproducto: " + el.getProducto() + " Cantidad: " + el.getCantidad());
                    }
                    break;

                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } while (op != 3 && op != 2 && op != 1);
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        em.close();
        emf.close();
    }

}

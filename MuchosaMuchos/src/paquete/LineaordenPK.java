/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author chris_000
 */
@Embeddable
public class LineaordenPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idOrden")
    private int idOrden;
    @Basic(optional = false)
    @Column(name = "idProducto")
    private int idProducto;

    public LineaordenPK() {
    }

    public LineaordenPK(int idOrden, int idProducto) {
        this.idOrden = idOrden;
        this.idProducto = idProducto;
    }

    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idOrden;
        hash += (int) idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LineaordenPK)) {
            return false;
        }
        LineaordenPK other = (LineaordenPK) object;
        if (this.idOrden != other.idOrden) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "paquete.LineaordenPK[ idOrden=" + idOrden + ", idProducto=" + idProducto + " ]";
    }
    
}
